import psycopg2

from database.connection_config import ServerConfig


class Db_connection:
    # Метод для подключения к базе данных
    @staticmethod
    def create_db_connection():
        conn = psycopg2.connect(dbname=ServerConfig.db_name, port=ServerConfig.port, user=ServerConfig.user,
                                    password=ServerConfig.password, host=ServerConfig.host)
        cursor = conn.cursor()
        return conn, cursor

# Метод для получения информации всей информации из таблицы (soldering)
    @staticmethod
    def select_all_from_soldering():
        conn, cursor = Db_connection.create_db_connection()
        cursor.execute("BEGIN;")
        request = "SELECT * from soldering"
        cursor.execute(request)
        result = cursor.fetchall()
        cursor.close()
        conn.close()
        return result

# Метод для добавления пункта информации в таблицу (soldering)
    @staticmethod
    def insert_item_from_soldering(request):
        conn, cursor = Db_connection.create_db_connection()
        values = request
        cursor.execute("BEGIN;")
        request = f'INSERT INTO soldering VALUES ({values["id"]}, ' \
                  f'{values["object_num"]}, {values["TS"]}, ' \
                  f'{values["P"]}, {values["time"]}, ' \
                  f'{values["length"]}, {values["pressure"]})'
        cursor.execute(request)
        cursor.execute("COMMIT;")
        cursor.close()
        conn.close()

# Метод для удаления одного пункта информации в таблице (soldering)
# В паратметре передаётся id элемента
    @staticmethod
    def delete_item_from_soldering(request):
        conn, cursor = Db_connection.create_db_connection()
        values = request
        cursor.execute("BEGIN;")
        request = f'DELETE FROM soldering ' \
                  f'WHERE id = {values};'
        cursor.execute(request)
        cursor.execute("COMMIT;")
        cursor.close()
        conn.close()

# Метод для получения информации определённой информации из таблицы (soldering)
# Параметр request - массив с данными о том какую информацию искать
    @staticmethod
    def select_custom_from_soldering(request):
        conn, cursor = Db_connection.create_db_connection()
        cursor.execute("BEGIN;")
        request = "SELECT * from soldering " \
                  f"WHERE {request[0]} = '{request[1]}' "   # request[0] - название колонки массива, request[1] - значение элемента
        cursor.execute(request)
        result = cursor.fetchall()
        cursor.close()
        conn.close()
        return result