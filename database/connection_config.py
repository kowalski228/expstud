# Данные для подключения к базе данных
class ServerConfig:
    db_name = 'ExpStud'
    user = 'postgres'
    port = 5432
    password = 'qwerty'
    server_host = 'localhost'
    host = 'localhost'
    timeout = 9999
