PGDMP     (                    y            ExpStud    12.4    12.4 "    4           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            5           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            6           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            7           1262    18259    ExpStud    DATABASE     �   CREATE DATABASE "ExpStud" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251';
    DROP DATABASE "ExpStud";
                postgres    false            �            1259    18297    discription    TABLE     a   CREATE TABLE public.discription (
    discrid integer NOT NULL,
    discription text NOT NULL
);
    DROP TABLE public.discription;
       public         heap    postgres    false            �            1259    18305 
   experement    TABLE     �   CREATE TABLE public.experement (
    manageid integer NOT NULL,
    objid integer NOT NULL,
    paramid integer NOT NULL,
    outid integer NOT NULL,
    discrid integer NOT NULL,
    expid integer NOT NULL
);
    DROP TABLE public.experement;
       public         heap    postgres    false            �            1259    18359    experement_expid_seq    SEQUENCE     �   ALTER TABLE public.experement ALTER COLUMN expid ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.experement_expid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 237539539
    CACHE 1
);
            public          postgres    false    207            �            1259    18279    managesystems    TABLE     �   CREATE TABLE public.managesystems (
    manageid integer NOT NULL,
    radkoef double precision[] NOT NULL,
    algtype text,
    algkoef double precision[] NOT NULL
);
 !   DROP TABLE public.managesystems;
       public         heap    postgres    false            �            1259    18269    objectparams    TABLE     "  CREATE TABLE public.objectparams (
    objid integer NOT NULL,
    typevvs character varying(50) NOT NULL,
    sizeofpipe text NOT NULL,
    widthofpipewall double precision,
    heightfloorm double precision,
    lengthfloorm double precision,
    widthfloorm double precision NOT NULL
);
     DROP TABLE public.objectparams;
       public         heap    postgres    false            �            1259    18289    outputparams    TABLE     �   CREATE TABLE public.outputparams (
    outid integer NOT NULL,
    firstelemtemp double precision[] NOT NULL,
    secondelemtemp double precision[] NOT NULL,
    deffects text,
    media bytea NOT NULL
);
     DROP TABLE public.outputparams;
       public         heap    postgres    false            �            1259    18262    technicalparametrs    TABLE       CREATE TABLE public.technicalparametrs (
    paramid integer NOT NULL,
    maxpower double precision NOT NULL,
    speedofheat double precision NOT NULL,
    stabletemp double precision NOT NULL,
    stabletime interval NOT NULL,
    gap double precision NOT NULL
);
 &   DROP TABLE public.technicalparametrs;
       public         heap    postgres    false            /          0    18297    discription 
   TABLE DATA           ;   COPY public.discription (discrid, discription) FROM stdin;
    public          postgres    false    206   %'       0          0    18305 
   experement 
   TABLE DATA           U   COPY public.experement (manageid, objid, paramid, outid, discrid, expid) FROM stdin;
    public          postgres    false    207   B'       -          0    18279    managesystems 
   TABLE DATA           L   COPY public.managesystems (manageid, radkoef, algtype, algkoef) FROM stdin;
    public          postgres    false    204   _'       ,          0    18269    objectparams 
   TABLE DATA           |   COPY public.objectparams (objid, typevvs, sizeofpipe, widthofpipewall, heightfloorm, lengthfloorm, widthfloorm) FROM stdin;
    public          postgres    false    203   |'       .          0    18289    outputparams 
   TABLE DATA           ]   COPY public.outputparams (outid, firstelemtemp, secondelemtemp, deffects, media) FROM stdin;
    public          postgres    false    205   �'       +          0    18262    technicalparametrs 
   TABLE DATA           i   COPY public.technicalparametrs (paramid, maxpower, speedofheat, stabletemp, stabletime, gap) FROM stdin;
    public          postgres    false    202   �'       8           0    0    experement_expid_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.experement_expid_seq', 3, true);
          public          postgres    false    208            �
           2606    18358    experement experement_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.experement
    ADD CONSTRAINT experement_pkey PRIMARY KEY (expid);
 D   ALTER TABLE ONLY public.experement DROP CONSTRAINT experement_pkey;
       public            postgres    false    207            �
           2606    18304    discription pk_discription 
   CONSTRAINT     ]   ALTER TABLE ONLY public.discription
    ADD CONSTRAINT pk_discription PRIMARY KEY (discrid);
 D   ALTER TABLE ONLY public.discription DROP CONSTRAINT pk_discription;
       public            postgres    false    206            �
           2606    18286    managesystems pk_managesystems 
   CONSTRAINT     b   ALTER TABLE ONLY public.managesystems
    ADD CONSTRAINT pk_managesystems PRIMARY KEY (manageid);
 H   ALTER TABLE ONLY public.managesystems DROP CONSTRAINT pk_managesystems;
       public            postgres    false    204            �
           2606    18276    objectparams pk_objectparams 
   CONSTRAINT     ]   ALTER TABLE ONLY public.objectparams
    ADD CONSTRAINT pk_objectparams PRIMARY KEY (objid);
 F   ALTER TABLE ONLY public.objectparams DROP CONSTRAINT pk_objectparams;
       public            postgres    false    203            �
           2606    18296    outputparams pk_outputparams 
   CONSTRAINT     ]   ALTER TABLE ONLY public.outputparams
    ADD CONSTRAINT pk_outputparams PRIMARY KEY (outid);
 F   ALTER TABLE ONLY public.outputparams DROP CONSTRAINT pk_outputparams;
       public            postgres    false    205            �
           2606    18266 (   technicalparametrs pk_technicalparametrs 
   CONSTRAINT     k   ALTER TABLE ONLY public.technicalparametrs
    ADD CONSTRAINT pk_technicalparametrs PRIMARY KEY (paramid);
 R   ALTER TABLE ONLY public.technicalparametrs DROP CONSTRAINT pk_technicalparametrs;
       public            postgres    false    202            �
           1259    18338    fkidx_44    INDEX     C   CREATE INDEX fkidx_44 ON public.experement USING btree (manageid);
    DROP INDEX public.fkidx_44;
       public            postgres    false    207            �
           1259    18339    fkidx_47    INDEX     @   CREATE INDEX fkidx_47 ON public.experement USING btree (objid);
    DROP INDEX public.fkidx_47;
       public            postgres    false    207            �
           1259    18340    fkidx_50    INDEX     B   CREATE INDEX fkidx_50 ON public.experement USING btree (paramid);
    DROP INDEX public.fkidx_50;
       public            postgres    false    207            �
           1259    18341    fkidx_53    INDEX     @   CREATE INDEX fkidx_53 ON public.experement USING btree (outid);
    DROP INDEX public.fkidx_53;
       public            postgres    false    207            �
           1259    18342    fkidx_56    INDEX     B   CREATE INDEX fkidx_56 ON public.experement USING btree (discrid);
    DROP INDEX public.fkidx_56;
       public            postgres    false    207            �
           2606    18313    experement fk_43    FK CONSTRAINT     ~   ALTER TABLE ONLY public.experement
    ADD CONSTRAINT fk_43 FOREIGN KEY (manageid) REFERENCES public.managesystems(manageid);
 :   ALTER TABLE ONLY public.experement DROP CONSTRAINT fk_43;
       public          postgres    false    2716    207    204            �
           2606    18318    experement fk_46    FK CONSTRAINT     w   ALTER TABLE ONLY public.experement
    ADD CONSTRAINT fk_46 FOREIGN KEY (objid) REFERENCES public.objectparams(objid);
 :   ALTER TABLE ONLY public.experement DROP CONSTRAINT fk_46;
       public          postgres    false    2714    203    207            �
           2606    18323    experement fk_49    FK CONSTRAINT     �   ALTER TABLE ONLY public.experement
    ADD CONSTRAINT fk_49 FOREIGN KEY (paramid) REFERENCES public.technicalparametrs(paramid);
 :   ALTER TABLE ONLY public.experement DROP CONSTRAINT fk_49;
       public          postgres    false    207    202    2712            �
           2606    18328    experement fk_52    FK CONSTRAINT     w   ALTER TABLE ONLY public.experement
    ADD CONSTRAINT fk_52 FOREIGN KEY (outid) REFERENCES public.outputparams(outid);
 :   ALTER TABLE ONLY public.experement DROP CONSTRAINT fk_52;
       public          postgres    false    207    2718    205            �
           2606    18333    experement fk_55    FK CONSTRAINT     z   ALTER TABLE ONLY public.experement
    ADD CONSTRAINT fk_55 FOREIGN KEY (discrid) REFERENCES public.discription(discrid);
 :   ALTER TABLE ONLY public.experement DROP CONSTRAINT fk_55;
       public          postgres    false    2720    207    206            /      x������ � �      0      x������ � �      -      x������ � �      ,      x������ � �      .      x������ � �      +      x������ � �     