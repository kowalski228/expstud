let discriptionId;
let discription;

let identifier;
let maxPower;
let speedOfHeat;
let stableTemp;
let stableTime;
let gap;

let managerId;
let radKoef;
let algorythmType;
let algorythmKoef;

let outId;
let firstElemTemp;
let secondElemTemp;
let deffects;
let media;

let objectId;
let typeVVS;
let sizeOfPipe;
let widthOfPipeWall;
let heightFloorM;
let lengthFloorM;
let widthFloorM;

let experimentId;
let expManageId;
let expObjId;
let expParamId;
let expOutId;
let expDiscriptionId;

function getSecondStep()
{
	discriptionId = document.getElementById("discrId").value
	discription = document.getElementById("disc").value
	document.getElementById("btnsInputDivId").remove();
	document.getElementById("inputDivId").remove();
	let dataForInput;
	dataForInput = '<div class = "delFormDiv" id = "delIdDiv"> <div class = "formInputDiv" id = "inputDivId"> <input type = "text" class = "inputForm" id = "id" placeholder = "Identifier"> </div>'
	dataForInput += '<div class = "formInputDiv" id = "inputDivId"> <input type = "text" class = "inputForm" id = "maxPow" placeholder = "Max power"> </div>'
	dataForInput += '<div class = "formInputDiv" id = "inputDivId"> <input type = "text" class = "inputForm" id = "speedHeat" placeholder = "Speed of heat"> </div>'
	dataForInput += '<div class = "formInputDiv" id = "inputDivId"> <input type = "text" class = "inputForm" id = "sTemp" placeholder = "Stable temp"> </div>'
	dataForInput += '<div class = "formInputDiv" id = "inputDivId"> <input type = "text" class = "inputForm" id = "sTime" placeholder = "Stable time"> </div>'
	dataForInput += '<div class = "formInputDiv" id = "inputDivId"> <input type = "text" class = "inputForm" id = "gap" placeholder = "Gap"> </div> </div>'
	dataForInput += '<div class = "btnsInputDiv" id = "btnsInputDivId"> <input type = "button" class = "formBtnAdd" id = "secondStepBtn" value = "NEXT STEP" onclick = "getThirdStep()"> </div>'
	document.querySelector(".inputMainForm").innerHTML = dataForInput
}

function getThirdStep()
{
	identifier = document.getElementById("id").value
	maxPower = document.getElementById("maxPow").value
	speedOfHeat = document.getElementById("speedHeat").value
	stableTemp = document.getElementById("sTemp").value
	stableTime = document.getElementById("sTime").value
	gap = document.getElementById("gap").value
	document.getElementById("btnsInputDivId").remove();
	document.getElementById("delIdDiv").remove();
	let dataForInput;
	dataForInput = '<div class = "delFormDiv" id = "delIdDiv"> <div class = "formInputDiv" id = "inputDivId"> <input type = "text" class = "inputForm" id = "manId" placeholder = "Manage id"> </div>'
	dataForInput += '<div class = "formInputDiv" id = "inputDivId"> <input type = "text" class = "inputForm" id = "radKf" placeholder = "Rad koef"> </div>'
	dataForInput += '<div class = "formInputDiv" id = "inputDivId"> <input type = "text" class = "inputForm" id = "algType" placeholder = "Algorythm type"> </div>'
	dataForInput += '<div class = "formInputDiv" id = "inputDivId"> <input type = "text" class = "inputForm" id = "algKf" placeholder = "Algorytm koef"> </div>'
	dataForInput += '<div class = "btnsInputDiv" id = "btnsInputDivId"> <input type = "button" class = "formBtnAdd" id = "thirdStepBtn" value = "NEXT STEP" onclick = "getFourthStep()"> </div>'
	document.querySelector(".inputMainForm").innerHTML = dataForInput
}

function getFourthStep()
{
	managerId = document.getElementById("manId").value
	radKoef = document.getElementById("radKf").value
	algorythmType = document.getElementById("algType").value
	algorythmKoef = document.getElementById("algKf").value
	document.getElementById("btnsInputDivId").remove();
	document.getElementById("delIdDiv").remove();
	let dataForInput;
	dataForInput = '<div class = "delFormDiv" id = "delIdDiv"> <div class = "formInputDiv" id = "inputDivId"> <input type = "text" class = "inputForm" id = "outId" placeholder = "Out id"> </div>'
	dataForInput += '<div class = "formInputDiv" id = "inputDivId"> <input type = "text" class = "inputForm" id = "fElemTemp" placeholder = "First element temp"> </div>'
	dataForInput += '<div class = "formInputDiv" id = "inputDivId"> <input type = "text" class = "inputForm" id = "sElemTemp" placeholder = "Second element temp type"> </div>'
	dataForInput += '<div class = "formInputDiv" id = "inputDivId"> <input type = "text" class = "inputForm" id = "deff" placeholder = "Deffects"> </div>'
	dataForInput += '<div class = "formInputDiv" id = "inputDivId"> <input type = "text" class = "inputForm" id = "med" placeholder = "Media"> </div>'
	dataForInput += '<div class = "btnsInputDiv" id = "btnsInputDivId"> <input type = "button" class = "formBtnAdd" id = "fourthStepBtn" value = "NEXT STEP" onclick = "getFifthStep()"> </div>'
	document.querySelector(".inputMainForm").innerHTML = dataForInput
}

function getFifthStep()
{
	outId = document.getElementById("outId").value
	firstElemTemp = document.getElementById("fElemTemp").value
	secondElemTemp = document.getElementById("sElemTemp").value
	deffects = document.getElementById("deff").value
	media = document.getElementById("med").value
	document.getElementById("btnsInputDivId").remove();
	document.getElementById("delIdDiv").remove();
	let dataForInput;
	dataForInput = '<div class = "delFormDiv" id = "delIdDiv"> <div class = "formInputDiv" id = "inputDivId"> <input type = "text" class = "inputForm" id = "objId" placeholder = "Object id"> </div>'
	dataForInput += '<div class = "formInputDiv" id = "inputDivId"> <input type = "text" class = "inputForm" id = "tVVS" placeholder = "Type of VVS"> </div>'
	dataForInput += '<div class = "formInputDiv" id = "inputDivId"> <input type = "text" class = "inputForm" id = "pipeSize" placeholder = "Size of pipe"> </div>'
	dataForInput += '<div class = "formInputDiv" id = "inputDivId"> <input type = "text" class = "inputForm" id = "pipeWallWidth" placeholder = "Width of pipe wall"> </div>'
	dataForInput += '<div class = "formInputDiv" id = "inputDivId"> <input type = "text" class = "inputForm" id = "heightFloor" placeholder = "Height floor M"> </div>'
	dataForInput += '<div class = "formInputDiv" id = "inputDivId"> <input type = "text" class = "inputForm" id = "lenFloor" placeholder = "Length floor M"> </div>'
	dataForInput += '<div class = "formInputDiv" id = "inputDivId"> <input type = "text" class = "inputForm" id = "widthFloor" placeholder = "Width floor M"> </div>'
	dataForInput += '<div class = "btnsInputDiv" id = "btnsInputDivId"> <input type = "button" class = "formBtnAdd" id = "fifthStepBtn" value = "ADD EXPERIMENT" onclick = "postText()"> </div>'
	document.querySelector(".inputMainForm").innerHTML = dataForInput
}

function getSixthStep()
{
	objectId = document.getElementById("objId").value
	typeVVS = document.getElementById("tVVS").value
	sizeOfPipe = document.getElementById("pipeSize").value
	widthOfPipeWall = document.getElementById("pipeWallWidth").value
	heightFloorM = document.getElementById("heightFloor").value
	lengthFloorM = document.getElementById("lenFloor").value
	widthFloorM = document.getElementById("widthFloor").value
	document.getElementById("btnsInputDivId").remove();
	document.getElementById("delIdDiv").remove();
}

function postText() {
    objectId = document.getElementById("objId").value
    typeVVS = document.getElementById("tVVS").value
    sizeOfPipe = document.getElementById("pipeSize").value
    widthOfPipeWall = document.getElementById("pipeWallWidth").value
    heightFloorM = document.getElementById("heightFloor").value
    lengthFloorM = document.getElementById("lenFloor").value
    widthFloorM = document.getElementById("widthFloor").value
    document.getElementById("btnsInputDivId").remove();
    document.getElementById("delIdDiv").remove();
    
    let soldering = {
        DiscrId: discriptionId,
        Discription: discription,
        ParamId: identifier,
        MaxPower: maxPower,
        SpeedOfHeat: speedOfHeat,
        StableTemp: stableTemp,
        StableTime: stableTime,
        Gap: gap,
        ManageId: managerId,
        RadKoef: radKoef, 
        AlgorytmType: algorythmType,
        AlgKoef: algorythmKoef,
        OutId: outId, 
        FirstElemTemp: firstElemTemp,
        SecondElemTemp: secondElemTemp,
        Deffects: deffects,
        Media: media,
        ObjId: objectId,
        TypeVVS: typeVVS,
        SizeOfPipe: sizeOfPipe,
        WidthOfPipeWall: widthOfPipeWall,
        HeightFloOrM: heightFloorM,
        LengthFloOrM: lengthFloorM,
        WidthFloOrM : widthFloorM
      };

    fetch('http://localhost:8080/', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json;charset=utf-8'
    },
    body: JSON.stringify(soldering)
 }).then(data => console.log(data));
 alert("Success")
}