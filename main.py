from aiohttp import web
import aiohttp_cors
import json

from database.db_connection import *
from server_logic.server_routes import set_server_routes


def parser_json(data):
    print(data['id'])


if __name__ == '__main__':
    app = web.Application()
    set_server_routes(app)
    web.run_app(app)


