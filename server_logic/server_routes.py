import aiohttp_cors

from server_logic.action import *


def set_server_routes(app):

    app.router.add_route("GET", "/", get_all)
    app.router.add_route("POST", "/", post_handle)
    app.router.add_route("GET", "/{column_name}/{index}", get_custom)
    app.router.add_route("DELETE", "/{index}", delete_handle)


    cors = aiohttp_cors.setup(app, defaults={
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        )
    })

    for route in list(app.router.routes()):
        print(route)
        cors.add(route)