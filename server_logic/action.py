import json

import numpy
from aiohttp import web

from database.db_connection import Db_connection
from filling_func import Db_filling


async def get_all(request):
    result = Db_filling.Custom_select()
    response = []
    for i in range(len(result)):
        temp_return = [
            # id, MaxPower, SpeedOfHeat, StableTemp
            str(result[i][0]), str(result[i][1][0]), str(result[i][1][1]), str(result[i][1][2]),
            # StableTime, Gap, TypeVVS, SizeOfPipe
            str(result[i][1][3]), str(result[i][1][4]), str(result[i][2][0]), str(result[i][2][1]),
            # WidthOfPipeWall, HeightFloOrM, LengthFloOrM, WidthFloOrM
            str(result[i][2][2]), str(result[i][2][3]), str(result[i][2][4]), str(result[i][2][5]),
            # RadKoef, AlgorytmType, AlgKoef, FirstElemTemp
            str(result[i][3][0]), str(result[i][3][1]), str(result[i][3][2]), str(result[i][4][0]),
            # SecondElemTemp, Deffects, Discription
            str(result[i][4][1]), str(result[i][4][2]), str(result[i][5][0])
        ]
        response += [temp_return.copy()]
    return web.json_response(response)


async def post_handle(request):
    data = json.loads(
        request.content._buffer[0].decode('utf-8').replace("'", '"'))  # Преобразование данных в json формат
    print(data)
    result = Db_filling.Deep_insert(data)  # Занесение поступивших данных во все зависимости sql
    print(result)
    print(Db_filling.Deep_select())
    return web.json_response(data)


async def delete_handle(request):
    data = request.match_info['index']
    Db_connection.delete_item_from_soldering(request.match_info['index'])
    return web.json_response(data)


async def get_custom(request):
    data = []
    data.append(request.match_info['column_name'])
    data.append(request.match_info['index'])
    buf = Db_connection.select_custom_from_soldering(data)
    response = []
    for i in range(len(buf)):
        response.append({
            "id": buf[i][0], "object_num": buf[i][1], "ts": buf[i][2], "P": buf[i][3],
            "time": buf[i][4], "length": buf[i][5], "pressure": buf[i][6]
        })
    return web.json_response(response)
