import psycopg2
import scipy.integrate as integrate
from filling_func import *
import numpy as np
import matplotlib.pyplot as plt

# TechnicalParams
    # ParamId,
    # MaxPower максимальная мощность индуктора (5 киловат, 7 киловат, 12 киловат),
    # SpeedOfHeat скорость нагрева изделия (до 10 градусов в сек),
    # StableTemp температура стабилизации (то есть, до какой температуры. 100-2500),
    # StableTime время стабилизации (до 10 секунд),
    # Gap расстояние (зазор) от 0 до 20 мм

    # ManageSystems
    # ManageId,
    # RadKoef коэф-ты излучательной способности приометров. от 0 до 1 с шагом 0.001,
    # AlgorythmType тип алгоритма (регулятор, искусственная, нейорнная сеть, нечёткая логика),
    # AlgKoef коэф-ты в алгоритмах управления (массив) (любые вещественные значения)?

    # ObjectParams
    # ObjId,
    # TypeVVS тип волновой сборки (1 волноводная труба - фланец, 2 труба - муфта, 3 труба - муфта - труба),
    # SizeOfPipe размер NxN,
    # WidthOfPipeWall необязательный параметр,
    # HeightFloOrM необязательный параметр,
    # LengthFloOrM необязательный параметр,
    # WidthFloOrM необязательный параметр


def launch_model(cur):
    data = np.array([], float)
    x = np.array([], float)
    for i in range(1, 1000):
        result = computation(i, 60000, 150, 45, 5, 1, 2, 24)
        data = np.append(data, result)  # Тестовый запуск
        x = np.append(x, i)

    plt.plot(x, data)
    plt.xlabel("t, секунды")
    plt.ylabel("T, градусы Цельсия")
    plt.show()
    return 0

# Q - Кол-во тепла
# F - Поперечное сечение трубы
# x - Расстояние от источника тепла
# c - Объемная теплоёмкость
# t - время (секунды)
# b - Коэффициент тепловой конвенции во внешнюю среду с поверхности стержня
# a - Коэффициент теплопроводности
# p - Периметр сечения
# Мгновенные источник нагрева в бесконечном стержне
# интеграл от 0 до t (Q/(F*c*p*sqrt(4*pi*a*t)) * exp(-(x)**2/4at - bt))dt
def computation(t, Q, F, c, x, b, a, p):
    result = integrate.quad(computation_integral, 0, t, args=(Q, F, c, x, b, a, p))
    print(f"Результат функции Computation: {result[0]}, погрешность: {result[1]}")
    return result[0]  # Возращается лишь первое значение


def computation_integral(Q, F, c, x, b, a, p, t):
    return float(Q) / (F * c * p * np.sqrt(4 * np.pi * a * t)) * np.exp(-(x ** 2) / (4 * a * t) - b * t)


if __name__ == '__main__':
    conn = psycopg2.connect(host="localhost", port=5432, database="ExpStud", user="postgres", password="qwerty")
    cur = conn.cursor()
    launch_model(cur)
