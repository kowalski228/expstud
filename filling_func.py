import psycopg2
from database.db_connection import Db_connection
import datetime

class Db_filling:
    @staticmethod
    def Custom_select():
        conn, cur = Db_connection.create_db_connection()
        cur.execute("SELECT ExpId, ManageId, ObjId, ParamId, OutId, DiscrId FROM Experement")
        result = list(cur.fetchall())
        for i in range(len(result)):
            result[i] = list(result[i])
        temp_return = [0] * 6
        operation = []
        for i in range(0, len(result)):
            temp_return[0] = result[i][0]

            cur.execute("SELECT RadKoef, AlgType, AlgKoef FROM " \
                        "ManageSystems ms WHERE ms.ManageId = " + str(result[i][1]))
            manage = cur.fetchone()
            temp_return[3] = manage

            cur.execute("SELECT TypeVVS, SizeOfPipe, WidthOfPipeWall, HeightFloOrM, LengthFloOrM, WidthFloOrM FROM " \
                        "ObjectParams op WHERE op.ObjId = " + str(result[i][2]))
            object = cur.fetchone()
            temp_return[2] = object

            cur.execute("SELECT MaxPower, SpeedOfHeat, StableTemp, StableTime, Gap FROM " \
                        "TechnicalParametrs tp WHERE tp.ParamId = " + str(result[i][3]))
            tech = cur.fetchone()
            temp_return[1] = tech

            cur.execute("SELECT FirstElemTemp, SecondElemTemp, Deffects FROM " \
                        "OutPutParams opp WHERE opp.OutId = " + str(result[i][4]))
            out = cur.fetchone()
            temp_return[4] = out

            cur.execute("SELECT Discription FROM Discription d WHERE d.DiscrId = " + str(result[i][5]))
            discr = cur.fetchone()
            temp_return[5] = discr

            operation += [temp_return.copy()]
        conn.close()
        return operation

    @staticmethod
    def Deep_select():
        conn, cur = Db_connection.create_db_connection()
        cur.execute("SELECT * FROM Discription")
        query_results1 = cur.fetchall()

        cur.execute("SELECT * FROM TechnicalParametrs")
        query_results2 = cur.fetchall()

        cur.execute("SELECT * FROM ManageSystems")
        query_results3 = cur.fetchall()

        cur.execute("SELECT * FROM OutPutParams")
        query_results4 = cur.fetchall()

        cur.execute("SELECT * FROM ObjectParams")
        query_results5 = cur.fetchall()

        cur.execute("SELECT ExpId, ManageId, ObjId, ParamId, OutId, DiscrId FROM Experement")
        query_results6 = cur.fetchall()

        conn.close()
        return query_results1, query_results2, query_results3, query_results4, query_results5, query_results6

    @staticmethod
    def Delete_experement(ExpId):
        conn, cur = Db_connection.create_db_connection()
        sql = "BEGIN"
        cur.execute(sql)
        sql = "SELECT ManageId, ObjId, ParamId, OutId, DiscrId FROM Experement exp WHERE exp.ExpId = " + str(ExpId)
        cur.execute(sql)
        sql = cur.fetchall()
        manageid = sql[0][0]
        objid = sql[0][1]
        paramid = sql[0][2]
        outid = sql[0][3]
        discrid = sql[0][4]
        sql = "DELETE FROM Experement exp WHERE exp.ExpId = " + str(ExpId)
        cur.execute(sql)
        sql = "DELETE FROM Discription d WHERE d.DiscrId = " + str(discrid)
        cur.execute(sql)
        sql = "DELETE FROM TechnicalParametrs tp WHERE tp.ParamId = " + str(paramid)
        cur.execute(sql)
        sql = "DELETE FROM ManageSystems ms WHERE ms.ManageId = " + str(manageid)
        cur.execute(sql)
        sql = "DELETE FROM OutPutParams opp WHERE opp.OutId = " + str(outid)
        cur.execute(sql)
        sql = "DELETE FROM ObjectParams op WHERE op.ObjId = " + str(objid)
        cur.execute(sql)
        sql = "COMMIT"
        cur.execute(sql)
        conn.close()

    @staticmethod
    def Deep_insert(data):
        conn, cur = Db_connection.create_db_connection()
        sql = "BEGIN"
        cur.execute(sql)
        d_insert = Db_filling.Discription_insert(cur, data['DiscrId'], data['Discription'])
        tp_insert = Db_filling.TechnicalParams_insert(cur, data['ParamId'], data['MaxPower'],
                                                           data['SpeedOfHeat'], data['StableTemp'],
                                                           data['StableTime'], data['Gap'])
        ms_insert = Db_filling.ManageSystems_insert(cur, data['ManageId'], data['RadKoef'],
                                                         data['AlgorytmType'], data['AlgKoef'])
        opp_insert = Db_filling.OutPutParams_insert(cur, data['OutId'], data['FirstElemTemp'],
                                                         data['SecondElemTemp'], data['Deffects'],
                                                         data['Media'])
        op_insert = Db_filling.ObjectParams_insert(cur, data['ObjId'], data['TypeVVS'],
                                                        data['SizeOfPipe'], data['WidthOfPipeWall'],
                                                        data['HeightFloOrM'], data['LengthFloOrM'],
                                                        data['WidthFloOrM'])
        exp_insert = Db_filling.Experement_insert(cur, data['ManageId'],
                                                       data['ObjId'], data['ParamId'],
                                                       data['OutId'], data['DiscrId'])
        sql = "COMMIT"
        cur.execute(sql)
        conn.close()
        return [d_insert, tp_insert, ms_insert, opp_insert, op_insert, exp_insert]

    @staticmethod
    def Discription_insert(cur, DiscrId, Discription):
        sql = "INSERT INTO Discription VALUES (" + str(DiscrId)
        sql = sql + ", '" + str(Discription) + "')"
        cur.execute(sql)

        sql = "SELECT * FROM Discription WHERE DiscrId = " + str(DiscrId)
        cur.execute(sql)
        return cur.fetchall()

    @staticmethod
    def TechnicalParams_insert(cur, ParamId, MaxPower, SpeedOfHeat, StableTemp, StableTime, Gap):
        StableTime = str(StableTime) + " second"
        sql = "INSERT INTO TechnicalParametrs VALUES (" + str(ParamId)
        sql = sql + ", " + str(MaxPower)
        sql = sql + ", " + str(SpeedOfHeat)
        sql = sql + ", " + str(StableTemp)
        sql = sql + ", '" + StableTime # Здесь должно быть время в формате time without time zone
        sql = sql + "', " + str(Gap) + ")"
        cur.execute(sql)

        sql = "SELECT * FROM TechnicalParametrs WHERE ParamId = " + str(ParamId)
        cur.execute(sql)
        return cur.fetchall()

    @staticmethod
    def ManageSystems_insert(cur, ManageId, RadKoef, AlgorytmType, AlgKoef):
        sql = "INSERT INTO ManageSystems VALUES (" + str(ManageId)
        sql = sql + ", '{" + str(RadKoef)
        sql = sql + "}', '" + str(AlgorytmType)
        sql = sql + "', '{" + str(AlgKoef) + "}')"
        cur.execute(sql)

        sql = "SELECT * FROM ManageSystems WHERE ManageId = " + str(ManageId)
        cur.execute(sql)
        return cur.fetchall()

    @staticmethod
    def OutPutParams_insert(cur, OutId, FirstElemTemp, SecondElemTemp, Deffects, Media):
        sql = "INSERT INTO OutPutParams VALUES (" + str(OutId)
        sql = sql + ", '{" + str(FirstElemTemp)
        sql = sql + "}', '{" + str(SecondElemTemp)
        sql = sql + "}', '" + str(Deffects)
        sql = sql + "', '" + str(Media) + "')"
        cur.execute(sql)

        sql = "SELECT * FROM OutPutParams WHERE OutId = " + str(OutId)
        cur.execute(sql)
        return cur.fetchall()

    @staticmethod
    def ObjectParams_insert(cur, ObjId, TypeVVS, SizeOfPipe, WidthOfPipeWall, HeightFloOrM, LengthFloOrM, WidthFloOrM):
        sql = "INSERT INTO ObjectParams VALUES (" + str(ObjId)
        sql = sql + ", " + str(TypeVVS)
        sql = sql + ", '" + str(SizeOfPipe)
        sql = sql + "', " + str(WidthOfPipeWall)
        sql = sql + ", " + str(HeightFloOrM)
        sql = sql + ", " + str(LengthFloOrM)
        sql = sql + ", " + str(WidthFloOrM) + ")"
        cur.execute(sql)

        sql = "SELECT * FROM ObjectParams WHERE ObjId = " + str(ObjId)
        cur.execute(sql)
        return cur.fetchall()

    @staticmethod
    def Experement_insert(cur, ManageId, ObjId, ParamId, OutId, DiscrId):
        sql = "INSERT INTO Experement (ManageId, ObjId, ParamId, OutId, DiscrId) VALUES ("
        sql = sql + str(ManageId)
        sql = sql + ", " + str(ObjId)
        sql = sql + ", " + str(ParamId)
        sql = sql + ", " + str(OutId)
        sql = sql + ", " + str(DiscrId) + ")"
        cur.execute(sql)

        sql = "SELECT ExpId, ManageId, ObjId, ParamId, OutId, DiscrId FROM Experement WHERE ManageId = " + str(ManageId)
        cur.execute(sql)
        return cur.fetchall()